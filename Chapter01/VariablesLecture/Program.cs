﻿using System;

namespace VariablesLecture
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            // x + 2
            // (1) + 2 = 3
            // (2) + 2 = 4

            int? x = null;
            double priceOfItem;
            string name;
            string message = "Hello";

            message = "Hi";

            x = 5;

            Console.WriteLine(message);

            Console.ReadKey();
        }
    }
}